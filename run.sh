#!/bin/sh

export PERLBREW_ROOT="${WERCKER_CACHE_DIR}/perl5"
if ! type perlbrew &>/dev/null; then
	error "perlbrew is not installed"
else
	info "perlbrew is installed"
	debug "$(perlbrew --version)"
	debug "$(perlbrew list)"
	debug "${PERLBREW_ROOT}"
	debug "$(ls -l ${PERLBREW_ROOT})"
fi

if [ -n "$WERCKER_PERLBREW_INSTALL" ]; then
	perlbrew install-multiple --verbose ${WERCKER_PERLBREW_INSTALL}
fi

if [ -n "$WERCKER_PERLBREW_EXEC" ]; then
	perlbrew exec ${WERCKER_PERLBREW_EXEC}
fi

if [ -n "$WERCKER_PERLBREW_CPANM" ]; then
	perlbrew exec cpanm ${WERCKER_PERLBREW_CPANM}
fi

if [ -n "$WERCKER_PERLBREW_TEST" ]; then
	perlbrew exec prove -lv
fi
